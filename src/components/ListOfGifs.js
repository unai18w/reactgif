import React, { useState, useEffect } from "react";
import Gif from "./Gif";
import searchGifs from "../services/searchGif";

export default function ListOfGifs({ params }) {
  const { keyword } = params;
  const [gifs, setGifs] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(
    function () {
      setLoading(true);
      searchGifs({ keyword }).then((gifs) => {
        setGifs(gifs);
        setLoading(false);
      });
    },
    [keyword]
  );

  if (loading) {
    return (
      <div>
        <h2>Loading...</h2>
      </div>
    );
  }

  return gifs.map(({ id, title, url }) => (
    <Gif key={id} url={url} title={title} id={id} />
  ));
}
