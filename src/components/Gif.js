import React from "react";
import "./Gif.css";

export default function Gif({ url, title, id }) {
  return (
    <a href={`#${id}`} className="Gif">
      <p>{title}</p>
      <img src={url} />
    </a>
  );
}
