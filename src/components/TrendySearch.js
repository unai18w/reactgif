import React, { useState, useEffect } from "react";
import getTrendyTerms from "../services/getTrendyTermsService";
import Category from "./Category";

export default function TrendySearch() {
  const [trends, setTrends] = useState([]);

  useEffect(
    function () {
      getTrendyTerms().then(setTrends);
    },
    [trends]
  );
  return trends.map((trend) => <Category category={trend} />);
}
