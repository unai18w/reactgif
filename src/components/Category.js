import React from "react";
import { Link } from "wouter";

export default function Category({ category }) {
  return <Link to={`/search/${category}`}>{category}</Link>;
}
