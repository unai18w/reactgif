const apiKey = "IkPAOudu1jVV972zZfpSGMaYnyoNowK2";

const fromApiResponseToTerms = (apiResponse) => {
  const { data = [] } = apiResponse;
  return data;
};

export default function getTrendyTerms() {
  const apiUrl = `api.giphy.com/v1/trending/searches?api_key=${apiKey}`;
  return fetch(apiUrl)
    .then((res) => res.json())
    .then(fromApiResponseToTerms);
}
