import "./App.css";
import React, { useState } from "react";
import ListOfGifs from "./components/ListOfGifs";
import TrendySearch from "./components/TrendySearch";
import { Route, useLocation } from "wouter";

function App() {
  const [keyword, setKeyword] = useState("");
  const [path, pushLocation] = useLocation();

  const handleSubmit = (evt) => {
    evt.preventDefault();
    pushLocation(`/search/${keyword}`);
  };

  const handleChange = (evt) => {
    evt.preventDefault();
    setKeyword(evt.target.value);
  };

  return (
    <div className="App">
      <h1>GIFS</h1>
      <form onSubmit={handleSubmit}>
        <input
          onChange={handleChange}
          type="text"
          value={keyword}
          placeholder="Busca un gif aquí..."
        />
      </form>
      <section className="App-content">
        <Route path="/search/:keyword" component={ListOfGifs} />
      </section>
      <section className="Trend">
        <h2>Lo más buscado</h2>
        <TrendySearch />
      </section>
    </div>
  );
}

export default App;
